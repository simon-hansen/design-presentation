export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyC8LQLe1CpEcpgxemrSaY8lkaZBd4AvEkA',
    authDomain: 'design-by-hoefer-hansen.firebaseapp.com',
    databaseURL: 'https://design-by-hoefer-hansen.firebaseio.com',
    projectId: 'design-by-hoefer-hansen',
    storageBucket: 'design-by-hoefer-hansen.appspot.com',
    messagingSenderId: '65174609070'
  }
};
