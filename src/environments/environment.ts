// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyC8LQLe1CpEcpgxemrSaY8lkaZBd4AvEkA',
    authDomain: 'design-by-hoefer-hansen.firebaseapp.com',
    databaseURL: 'https://design-by-hoefer-hansen.firebaseio.com',
    projectId: 'design-by-hoefer-hansen',
    storageBucket: 'design-by-hoefer-hansen.appspot.com',
    messagingSenderId: '65174609070'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
