import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DesignPresentationComponent} from './components/design-presentation/design-presentation.component';

const routes: Routes = [
  { path: 'projects',   redirectTo: '/projects/show-exclusiv-website', pathMatch: 'full' },
  { path: 'projects/:token', component: DesignPresentationComponent, pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
