export interface Project {
  versions: ProjectData[];
}

export interface ProjectData {
  designFullURL: string;
  designMacURL: string;
  title: string;
  version: string;
  specs: Spec;
}

export interface Color {
  colorCode: string;
  description?: string;
}

export interface Spec {
  colors: Color[];
  logoURL: string;
}
