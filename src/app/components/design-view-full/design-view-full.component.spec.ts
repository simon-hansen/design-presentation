import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignViewFullComponent } from './design-view-full.component';

describe('DesignViewFullComponent', () => {
  let component: DesignViewFullComponent;
  let fixture: ComponentFixture<DesignViewFullComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignViewFullComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignViewFullComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
