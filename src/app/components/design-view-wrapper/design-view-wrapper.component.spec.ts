import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignViewWrapperComponent } from './design-view-wrapper.component';

describe('DesignViewWrapperComponent', () => {
  let component: DesignViewWrapperComponent;
  let fixture: ComponentFixture<DesignViewWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignViewWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignViewWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
