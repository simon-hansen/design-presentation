import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignViewMacComponent } from './design-view-mac.component';

describe('DesignViewMacComponent', () => {
  let component: DesignViewMacComponent;
  let fixture: ComponentFixture<DesignViewMacComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignViewMacComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignViewMacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
