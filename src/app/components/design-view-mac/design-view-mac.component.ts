import {Component, Input, OnInit} from '@angular/core';
import {LoaderService} from '../../services/loader/loader.service';

@Component({
  selector: 'app-design-view-mac',
  templateUrl: './design-view-mac.component.html',
  styleUrls: ['./design-view-mac.component.scss']
})
export class DesignViewMacComponent implements OnInit {

  @Input() src: string;

  constructor(public loaderService: LoaderService) { }

  ngOnInit() {
  }

}
