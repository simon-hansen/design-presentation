import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignPresentationComponent } from './design-presentation.component';

describe('DesignPresentationComponent', () => {
  let component: DesignPresentationComponent;
  let fixture: ComponentFixture<DesignPresentationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignPresentationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignPresentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
