import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignSpecsComponent } from './design-specs.component';

describe('DesignSpecsComponent', () => {
  let component: DesignSpecsComponent;
  let fixture: ComponentFixture<DesignSpecsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignSpecsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignSpecsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
