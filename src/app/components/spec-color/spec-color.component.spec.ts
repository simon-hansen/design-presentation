import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecColorComponent } from './spec-color.component';

describe('SpecColorComponent', () => {
  let component: SpecColorComponent;
  let fixture: ComponentFixture<SpecColorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecColorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
