import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-spec-color',
  templateUrl: './spec-color.component.html',
  styleUrls: ['./spec-color.component.scss']
})
export class SpecColorComponent implements OnInit {

  @Input() hex: string;
  @Input() desc: string;


  constructor() { }

  ngOnInit() {
  }

}
